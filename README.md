# Whiney Cat Audio Website
Whiney Cat Audio is a Seattle recording studio, whose aim is to highlight their services, studio space, and past work. The site is responsive with JavaScript fallbacks, accessibility features, and print stylings.  

February 2018

## Demo
https://www.whineycataudio.com

## My Roles
 - Co-Design
 - User-Experience
 - Front-End Development

## Tools/Skills Used
 - HTML5
 - CSS3
 - JavaScript
